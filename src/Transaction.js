import React, { Fragment } from 'react';
import { FaTrash } from 'react-icons/fa';
import './Transaction.css';
const Transaction = ({description, amount, account1, account2, date, id, onDeleteTransaction}) => (
  <Fragment>
    <div className="row">
      <h3>{date}</h3>
    </div>
    <div className="row">
      <div className="double-column">
        <span>{description}</span>
        <span className="credit">{account1}</span>
      </div>
      <div className="double-column">
        <span>R$ {amount}</span>
        <span className="debit">{account2}</span>
      </div>
      <div className="column">
        <button
          className="pure-button button-error"
          aria-label="delete"
          onClick={() => onDeleteTransaction(id)}>
            <FaTrash />
        </button>
      </div>
    </div>
  </Fragment>
);

export default Transaction;
