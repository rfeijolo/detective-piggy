import React, { Fragment } from 'react';
import './TransactionList.css';
import Transaction from './Transaction';

const TransactionList = ({transactions, onDeleteTransaction}) => {
  const hasTransactions = transactions.length > 0;
  const transactionElements = transactions.map(transaction =>
    <Transaction
      description={transaction.description}
      amount={transaction.amount}
      account1={transaction.account1}
      account2={transaction.account2}
      date={transaction.date}
      key={transaction.id}
      id={transaction.id}
      onDeleteTransaction={onDeleteTransaction}/>
  );
  return (<div>
    {hasTransactions ? (
      <Fragment>
        <h2> Latest transactions </h2>
        <div className='list-container'>
          {transactionElements}
        </div>
      </Fragment>
    ) : (
      <p> You have not registered any transactions yet. </p>
    )}
  </div>);
};

export default TransactionList;
