import './TransactionForm.css';
import React, { Component } from 'react';
import Autosuggest from 'react-autosuggest';
import generateUuid from './uuid';


const initialState = {
  description: '',
  amount: '',
  account1: '',
  account2: '',
  suggestions: [],
};

const getSuggestionValue = suggestion => suggestion.description;

const renderSuggestion = suggestion => (
  <div>
    {suggestion.description}
  </div>
);

class TransactionForm extends Component {
  constructor(props) {
    super(props);

    const [today] = new Date().toISOString().split('T');

    this.state = {
      ...initialState,
      date: today,
    };
  }

  getSuggestions = value => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;

    return inputLength === 0 ? [] : this.props.transactions.filter(
      transaction => transaction.description.toLowerCase().slice(0, inputLength) === inputValue
    );
  };

  handleSuggestionsFetchRequested = (event) => {
    this.setState({
      suggestions: this.getSuggestions(event.value)
    });
  }

  handleSuggestionSelected = (event, { suggestion }) => {
    const suggestionWithoutDate = Object.keys(suggestion)
      .filter(key => key !== 'date')
      .reduce((obj, key) => ({...obj, [key]: suggestion[key]}), {});
    this.setState({ ...suggestionWithoutDate });
  };

  handleSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.setState({...initialState});
    this.descriptionInput.focus();
    this.props.onSaveTransaction({...this.state, id: generateUuid() });
  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.id;

    this.setState({
      [name]: value
    });
  }

  handleFocus = (event) => {
    event.target.select();
  }

  render () {
    return (
      <div>
        <h2>Add transaction</h2>
        <form onSubmit={this.handleSubmit}>
          <fieldset>
            <div className="control-group">
              <label htmlFor="description">Description</label>
              <Autosuggest
                suggestions={this.state.suggestions}
                onSuggestionsFetchRequested={this.handleSuggestionsFetchRequested}
                onSuggestionsClearRequested={this.handleSuggestionsClearRequested}
                onSuggestionSelected={this.handleSuggestionSelected}
                getSuggestionValue={getSuggestionValue}
                renderSuggestion={renderSuggestion}
                ref={(autosuggest) => { if(!autosuggest) return; this.descriptionInput = autosuggest.input }}
                inputProps={{
                  value: this.state.description,
                  onChange: this.handleInputChange,
                  onFocus: this.handleFocus,
                  autoFocus: true,
                  required: true,
                  autoCapitalize: 'none',
                  id: 'description',
                  name: 'description',
                }}
                id='description-suggestion' />
            </div>
            <div className="control-group">
              <label htmlFor="amount">Amount</label>
              <input
                type="number"
                id="amount"
                value={this.state.amount}
                step="0.01"
                required
                autoCapitalize="none"
                onChange={this.handleInputChange}
                onFocus={this.handleFocus}/>
            </div>
            <div className="control-group credit">
              <label htmlFor="account1">Account 1</label>
              <input
                type="text"
                id="account1"
                value={this.state.account1}
                className="credit"
                required
                autoCapitalize="none"
                onChange={this.handleInputChange}
                onFocus={this.handleFocus}/>
            </div>
            <div className="control-group debit">
              <label htmlFor="account2">Account 2</label>
              <input
                type="text"
                id="account2"
                value={this.state.account2}
                className="debit"
                required
                autoCapitalize="none"
                onChange={this.handleInputChange}
                onFocus={this.handleFocus}/>
            </div>
            <div className="control-group">
              <label htmlFor="date">Date</label>
              <input
                required
                value={this.state.date}
                type="date"
                id="date"
                pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}"
                onChange={this.handleInputChange}
                onFocus={this.handleFocus}/>
            </div>
            <button className="pure-button">Save</button>
          </fieldset>
        </form>
      </div>
    );
  }
}

export default TransactionForm;
