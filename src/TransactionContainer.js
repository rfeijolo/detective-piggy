import React, { Component } from 'react';
import TransactionForm from './TransactionForm';
import TransactionList from './TransactionList';
import { get, set } from 'idb-keyval';

class TransactionContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      transactions: [],
    }
  }

  componentDidMount() {
    get('transactions').then(savedTransactions => {
      const transactions = savedTransactions || [];
      this.setState(previousState => ({
        transactions: [...previousState.transactions, ...transactions]
      }));
    });
  }

  handleSaveTransaction = (transaction) => {
    this.setState(previousState => {
      const newState = {
        transactions: [...previousState.transactions, transaction],
      };
      set('transactions', newState.transactions);
      return newState;
    });
  }

  handleDeleteTransaction = (transactionId) => {
    this.setState(previousState => {
      const newState = {
        transactions: previousState.transactions
          .filter(transaction => transaction.id !== transactionId)
      };
      set('transactions', newState.transactions);
      return newState;
    });
  }

  render() {
    return (
      <div>
        <TransactionForm
          transactions={this.state.transactions}
          onSaveTransaction={this.handleSaveTransaction} />
        <TransactionList
          transactions={this.state.transactions}
          onDeleteTransaction={this.handleDeleteTransaction} />
      </div>
    );
  }
}

export default TransactionContainer;
